package de.thm.mni.rsnr66;

import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class ThermaltouchWebSocketClient extends org.java_websocket.client.WebSocketClient {

    public ThermaltouchWebSocketClient(URI serverUri) throws InterruptedException {
        super(serverUri);
        connectBlocking();
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        System.out.println("Connected to server");
    }

    @Override
    public void onMessage(String s) {
        System.out.println("Received message: " + s);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        System.out.println("Disconnected from server with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onError(Exception e) {
        System.err.println("An error occurred:" + e);
    }
}
