package de.thm.mni.rsnr66.event_handlers;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import de.thm.mni.rsnr66.ExampleMod;
import de.thm.mni.rsnr66.ThermaltouchHelper;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.util.Objects;
import java.util.function.Supplier;

@Mod.EventBusSubscriber(modid = ExampleMod.MODID)
public class CommandEventHandler {

    private static final Logger LOGGER = LogManager.getLogger();

    @SubscribeEvent
    public static void onCommandsRegister(RegisterCommandsEvent event) {
        CommandDispatcher<CommandSourceStack> dispatcher = event.getDispatcher();
        LOGGER.info("Registering client-side commands");
        dispatcher.register(
                Commands.literal("thermaltouchinit")
                    .then(Commands.argument("ipaddress", StringArgumentType.word())
                            .then(Commands.argument("port", StringArgumentType.word())
                                .executes(context -> executeMyCommand(context,
                                    StringArgumentType.getString(context, "ipaddress"),
                                    StringArgumentType.getString(context, "port")
                                ))
                            )
                    )
        );
    }

    private static int executeMyCommand(CommandContext<CommandSourceStack> context, String ip, String port) {
        String uri = String.format("ws://%s:%s/", ip, port);
        Player player = context.getSource().getPlayer();
        if( player == null)
            return 0;
        player.sendSystemMessage(Component.literal(String.format("Trying to connect to %s", uri)));

        if (ThermaltouchHelper.getInstance().connect(uri))
            player.sendSystemMessage(Component.literal("Successfully connected to thermal touch controller!"));
        else
            player.sendSystemMessage(Component.literal("Error during connection to thermal touch controller!"));

        return 1;
    }
}
