package de.thm.mni.rsnr66.event_handlers;

import de.thm.mni.rsnr66.ThermaltouchHelper;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

public class FireWaterHitEventListener {

    int notBurningCounter = 5;
    boolean burning = false;
    boolean water = false;

    @SubscribeEvent
    public void waterFireTest(LivingEvent.LivingTickEvent event) {
        if (event.getEntity() instanceof Player player) {
            testForBurning(player);
            testForWater(player);
        }
    }

    private void testForBurning(Player player) {
        if (player.isOnFire()) {
            if (!burning) {
                System.out.println("Player started burning");
                ThermaltouchHelper.getInstance().startHeat();
                burning = true;
            }
            notBurningCounter = 0;
        } else {
            notBurningCounter = Math.min(++notBurningCounter, 6);
            if (notBurningCounter == 5) {
                System.out.println("Player stopped burning");
                ThermaltouchHelper.getInstance().stopHeat();
                burning = false;
            }
        }

    }

    private void testForWater(Player player) {
        if (player.isInWater()) {
            if (!water) {
                System.out.println("Play entered water!");
                ThermaltouchHelper.getInstance().startCold();
                water = true;
            }
        } else {
            if (water) {
                System.out.println("Play left water!");
                ThermaltouchHelper.getInstance().stopCold();
                water = false;
            }
        }
    }

    @SubscribeEvent
    public void hitTakenEvent(LivingHurtEvent event) {
        if (event.getEntity() instanceof Player player)
            ThermaltouchHelper.getInstance().playerHit();
    }
}
