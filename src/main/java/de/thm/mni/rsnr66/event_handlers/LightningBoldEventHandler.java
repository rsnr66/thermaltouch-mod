package de.thm.mni.rsnr66.event_handlers;

import de.thm.mni.rsnr66.ExampleMod;
import de.thm.mni.rsnr66.ThermaltouchHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.LightningBolt;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.entity.EntityJoinLevelEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber(modid = ExampleMod.MODID)
public class LightningBoldEventHandler {

    private static final Logger LOGGER = LogManager.getLogger();
    private static long lastLightningBolt = 0;

    @SubscribeEvent
    public static void onLightningStrike(EntityJoinLevelEvent event) {
        // Lightning bolts always spawn in pairs. To prevent a double execution of the event add a 100ms lock for the function
        if ((System.currentTimeMillis() - lastLightningBolt) > 100)
            if (event.getEntity() instanceof LightningBolt lightningBolt) {
                if (Minecraft.getInstance().player != null) {
                    if (lightningBolt.distanceTo(Minecraft.getInstance().player) < 2) {
                        LOGGER.info("Lightning Bolt hit Player");
                        ThermaltouchHelper.getInstance().lightning();
                    } else {
                        LOGGER.info("Lightning Bolt missed Player");
                    }
                }
                lastLightningBolt = System.currentTimeMillis();
            }
    }
}
