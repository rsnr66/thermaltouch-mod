package de.thm.mni.rsnr66.event_handlers;

import de.thm.mni.rsnr66.ThermaltouchHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.level.ExplosionEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;

public class CreeperAndExplosionEventListener {

    private static final Logger LOGGER = LogManager.getLogger();


    int recentSwellDir = -1;
    long lastCrreperFoundTick = System.currentTimeMillis();
    long lastRecentExplosion = System.currentTimeMillis();

    @SubscribeEvent
    public void onCreeperUpdate(LivingEvent.LivingTickEvent event) {
        if (event.getEntity() instanceof Creeper creeper) {
            if(System.currentTimeMillis() - lastCrreperFoundTick > 1000)
                recentSwellDir = -1;

            // Check if the creeper is nearby
            if(Minecraft.getInstance().player != null) {
                if (creeper.distanceTo(Minecraft.getInstance().player) < 10) {
                    lastCrreperFoundTick = System.currentTimeMillis();
                    if (recentSwellDir != creeper.getSwellDir()) {
                        if (recentSwellDir == -1)
                            ThermaltouchHelper.getInstance().creeperSwell();
                        else
                            ThermaltouchHelper.getInstance().creeperStopSwell();
                        recentSwellDir = creeper.getSwellDir();
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void onExplosion(ExplosionEvent event) {
        LOGGER.info("Explosion triggered");
        if(System.currentTimeMillis() - lastRecentExplosion > 1000) {
            ThermaltouchHelper.getInstance().explosion();
            lastRecentExplosion = System.currentTimeMillis();
        }
    }

}
