package de.thm.mni.rsnr66.event_handlers;

import de.thm.mni.rsnr66.ThermaltouchHelper;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.entity.living.MobEffectEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.logging.log4j.LogManager;

import java.util.logging.Logger;

public class NauseaEffectHandler {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger();


    @SubscribeEvent
    public void onNauseaEvent(MobEffectEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event instanceof MobEffectEvent.Added effectAddedEvent) {
                if (effectAddedEvent.getEffectInstance().getDescriptionId().equals("effect.minecraft.nausea")) {
                    // Do nausea stuff
                    LOGGER.info("Nausea detected");
                    ThermaltouchHelper.getInstance().nauseaStart();
                }
            } else if (event instanceof MobEffectEvent.Remove | event instanceof MobEffectEvent.Expired) {
                if (event.getEffectInstance() != null)
                    if (event.getEffectInstance().getDescriptionId().equals("effect.minecraft.nausea")) {
                        // Do nausea end stuff
                        LOGGER.info("Nausea ended");
                        ThermaltouchHelper.getInstance().nauseaStop();
                    }
            }
        }
    }
}
