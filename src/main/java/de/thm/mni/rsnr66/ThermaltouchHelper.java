package de.thm.mni.rsnr66;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;

public class ThermaltouchHelper {

    // Instance variables
    private ThermaltouchWebSocketClient client;
    private static ThermaltouchHelper instance;

    // Public method to retrieve instance
    public static ThermaltouchHelper getInstance() {
        if (instance == null)
            instance = new ThermaltouchHelper();
        return instance;
    }

    // Constructor
    public ThermaltouchHelper() {
        client = null;
    }

    // Private methods
    private void send(String msg) {
        if (client != null)
            client.send(msg);
    }

    // Public methods
    public boolean connect(String uriString) {
        try {
            client = new ThermaltouchWebSocketClient(new URI(uriString));
        } catch (InterruptedException e) {
            System.err.printf("Error connecting to %s", uriString);
            return false;
        } catch (URISyntaxException e) {
            System.err.printf("Error in URI %s", uriString);
            return false;
        }
        return true;
    }

    public void startHeat() {
        send("heat_start");
    }

    public void stopHeat() {
        send("heat_stop");
    }

    public void startCold() {
        send("cold_start");
    }

    public void stopCold() {
        send("cold_stop");
    }

    public void creeperSwell() {
        send("creeper_swell");
    }

    public void creeperStopSwell() {
        send("creeper_stop_swell");
    }

    public void explosion() {
        send("explosion");
    }

    public void nauseaStart() {
        send("nausea_start");
    }

    public void nauseaStop() {
        send("nausea_stop");
    }

    public void lightning() {
        send("lightning");
    }

    public void playerHit() {
        send("vib");
    }

}
